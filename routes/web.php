<?php

use App\Http\Controllers\Backend\CategoryController;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use App\Http\Controllers\CategoriesController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\UsersController;
use App\Http\Controllers\MemberController;
use App\Http\Controllers\Backend\DashboardController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function(){
    return view('welcome');
});

 Route::get('/', 'HomeController@index')->name('home');
Route::prefix('users')->name('users.')->group(function(){   
    Route::get('/', [UsersController::class, 'index'])->name('index');

    Route::get('/add', [UsersController::class, 'add'])->name('add');

    Route::post('/add', [UsersController::class, 'postAdd'])->name('post-add');

    Route::get('/edit/{id}', [UsersController::class, 'getEdit'])->name('edit');

    Route::post('/edit/{id}', [UsersController::class, 'postEdit'])->name('post-edit');

    Route::get('/delete/{id}', [UsersController::class, 'delete'])->name('delete');

});

Route::namespace('Backend')->prefix('backend')->group(function() {
    Route::get('/', [DashboardController::class, 'index'] )->name('backend.dashboard.index');
    Route::get('product', 'ProductsController@index')->name('backend.products.index');
    Route::get('category', 'CategoryController@index')->name('backend.category.index');
    Route::get('news', 'NewsController@index')->name('backend.news.index');
    Route::get('users', 'UsersController@index')->name('backend.users.index');
}); 