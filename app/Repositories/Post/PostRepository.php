<?php
namespace App\Repositories\Post;

use App\Repositories\BaseRepository;

class PostRepository extends BaseRepository implements PostRepositoryInterface
{
    /**
     * Get Model Post
     */
    public function getModel()
    {
        return \App\Models\Post::class;
    }

    public function getUsers()
    {
        return $this->model->select('users')->get();
    }
    public function getAll($attributes = [''])
    {
        if(empty($attributes[''])) $attributes['']='';
        
        return $this->model->where('fullname', 'LIKE', '%'.$attributes[''].'%')->get();
    }
}
