<?php
namespace App\Repositories;
use App\Repositories\RepositoryInterface;

abstract class BaseRepository implements RepositoryInterface
{
    protected $model;

    public function __construct()
    {
        $this->setModel();
    }

    /**
     * Get Model
     * @return mixed
     */
    abstract public function getModel();

    /**
     * Set Model
     */
    public function setModel()
    {
        $this->model = app()->make($this->getModel());
    }

    /**
     * Get One Record
     * @return mixed
     */
    public function find($id)
    {
        $result = $this->model->find($id);
        return $result;
    }

     /**
     * Create
     * @param array $attribute
     * @return mixed
     */
    public function create($attributes = [])
    {
        return $this->model->create($attributes);
    }

    /**
     * Update
     * @param $id
     * @param array $attribute
     * @return mixed
     */
    public function update($id, $attribute = [])
    {
        $result = $this->model->find($id);
        if($result)
        {
            $result->update($attribute);
            return $result;
        }

        return false;
    }

    /**
     * Delete
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        $result = $this->model->find($id);
        if($result)
        {
            $result->delete();
            return $result;
        }
        return false;
    }
}
