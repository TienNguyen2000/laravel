<?php
namespace App\Models;

use App\Http\Controllers\Api\PostController;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = "users";
    public $timestamps = false;
    protected $fillable = [
        'fullname',
        'email',
        'create_at',
        'group_id',
        'update_id',
    ];

    /**
     * Get the categories that owns the Post
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function categories()
    {
        return $this->belongsTo(PostController::class, 'users');
    }
}
?>
