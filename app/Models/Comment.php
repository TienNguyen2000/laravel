<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    public function authorize()
    {
    $comment = Comment::find($this->route('comment'));
 
    return $comment && $this->user()->can('update', $comment);
    }
    /**
    * Get the error messages for the defined validation rules.
    *
    * @return array
    */
    public function messages()
    {
        return [
            'fullname.required' => 'A title is required',
            'email.required' => 'A message is required',
    ];
}
}