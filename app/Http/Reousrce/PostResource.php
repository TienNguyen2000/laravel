<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'fullname' => $this->fullname,
            'email' => $this->email,
            'group_id' => $this->group_id,
            'creat_at' => $this->create_at,
            'update_at' => $this->update_at,
        ];
    }
}
