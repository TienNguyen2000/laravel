<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'POST' : 
                return [
                    'fullname' => 'required|min:10|max:255',
                    'email' => 'required',
                    'group_id' => 'required',
                    'create_at' => 'required|boolean',
                    'update_at' => 'required',
                ];
            case 'PUT' :
                return [
                    'fullname' => 'min:10|max:255',
                    'email' => 'required',
                    'group_id' => 'required',
                    'create_at' => 'required|boolean',
                    'update_at' => 'required',
                ];
        }
        
    }

    public function messages()
    {
        return [
            'fullname.required' => 'Title không được để trống',
            'fullname.min' => 'Title phải nhiều hơn 10 ký tự',
            'fullname.max' => 'Title không được vượt quá 255 ký tự',
        ];
    }
}
