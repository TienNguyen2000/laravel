
<!DOCTYPE html>
<html>
<head>
    <title>@yield('page_title')</title>
    <meta name="viewport" content="width=device-width" initial-scale="1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <style>
        * {
            margin: 0;
            padding: 0;
        }
        
        .top {
            width: 100%;
            background: black;
            height: 50px;
        }
        
        .footer {
            width: 100%;
            background: black;
            height: 50px;
            clear: both;
        }
        
        .left,
        .right {
            float: left;
            min-height: 600px;
            display: block;
        }
        
        .left {
            width: 200px;
            background: #f8f8f8;
            min-height: 600px;
        }
        
        .right {
            float: left;
            width: 800px;
        }
        
        ul {
            list-style-type: none;
        }
        
        ul li a {
            text-decoration: none;
            border-bottom: 1px solid #f1f1f1;
            display: block;
            line-height: 28px;
            padding-left: 10px;
            color: red;
        }
    </style>
</head>

<body>
    <?php

    ?>
    <div class="top"></div>
    <div class="left">
        <ul>
            <li>
                <a href="{{ route('backend.dashboard.index') }}">Dashboard</a>
            </li>
            <li>
                <a href="{{ route('backend.products.index') }}">Products</a>
            </li>
            <li>
                <a href="{{ route('backend.category.index') }}">Category</a>
            </li>
            <li>
                <a href="{{ route('backend.news.index') }}">News</a>
            </li>
            <li>
                <a href="{{ route('backend.users.index') }}">Users</a>
            </li>
        </ul>
    </div>
    <div class="right">
        @yield('main')
    </div>
    <div class="footer"></div>
</body>

</html>
