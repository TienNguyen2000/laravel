@extends('layouts.master')

@section('page_title')
    Category
@endsection

@section('main')
<h2>Categories</h2>
<div class="gird-view">
    @if (!empty ($usersList))
    <table border="1" style="width: 600px">
        @foreach ($usersList as $key => $item)
        <tr>
            <td>{{$key+1}}</td>
            <td>{{$item->fullname}}</td>
            <td>{{$item->email}}</td>
            <td>{{$item->create_at}}</td>
        </tr>
        @endforeach
    </table>
    @endif
</div>
@endsection