@extends('layouts.master')

@section('page_title')
    User
@endsection
<style>
    .pagination{
        display: flex;
        align-items: center;
        justify-content: center;
    }
    .page-item{
        margin-right: 10px;
    }
</style>
@section('main')
<h2>User</h2>
<a href="{{ route('users.add') }}" class="btn btn-primary">Thêm người dùng</a>
<hr />
<div class="gird-view">
    @if (!empty ($usersList))
    <table border="1" style="width: 600px">
        @foreach ($usersList as $key => $item)
        <tr>
            <td>{{$key+1}}</td>
            <td>{{$item->fullname}}</td>
            <td>{{$item->email}}</td>
            <td>{{$item->create_at}}</td>
            <td><a href="{{route('users.edit', ['id'=>$item->id])}}">Sửa</a></td>
            <td><a onclick="return confirm('Bạn có chắc chắn muốn xóa?')" href="{{route('users.delete', ['id'=>$item->id])}}">Xóa</a></td>
        </tr>
        @endforeach
    </table>
    @endif
    <div class="paginator" style="display: flex">
    {{$usersList->links()}}
    </div>
</div>
@endsection